# Livebase Gradle plugin #

This plugin for gradle allows one to interact with one or more Livebase Cloudlet in a gradle
project.

- Apply the plugin in your `build.gradle` file and configure the needed properties (Gradle
  configuration below)

Due to the use of `connectionApi` to interact with the cloudlet, when new APIs are released, your
plugin version must be updated or connection cannot be established. To check for compatibility use
the `printApi` task

## Sample project

Create a project and use this template as the `build.gradle` file (__compatibility: Gradle 4.9+__):

### Configuration

```groovy
buildscript {
  repositories {
    mavenCentral()
  }

  dependencies {
    // Use latest plugin version compatible with API, use task printApi to get API version
    classpath 'com.fhoster.livebase:livebase-gradle-plugin:2.0.+'
  }
}

apply plugin: 'com.fhoster.livebase'

// Id of cloudlet to manage
def id = 1
// Or name
def cloudlet = 'NewCloudlet1'
def model = 'Model1'

/**
 * Global options for all tasks, except from server configuration that is specified only here,
 * other configurations replace every MANDATORY option for every single task
 */
livebase {
  /**
   * This is the server, either by setting servantUrl manually,
   * or invoking one of the following methods, default is production() [OPTIONAL]
   * [production(), validation(), servantUrl = <url>]
   */
  production()

  // Username and password for server [MANDATORY]
  username = '****'
  password = '****'

  // This is the ID of the cloudlet [OPTIONAL, this or name needed]
  cloudletId = id
  // This is the name of the cloudlet [OPTIONAL, this or id needed]
  cloudletName = cloudlet
  // This is the name of the model [OPTIONAL]
  modelName = model
  // This is the name of plugin [OPTIONAL, needed if PluginTasks are used]
  // Can be a regex [default=false]
  pluginsName = 'test.jar'
  // SAME AS pluginsName 'test.jar', false
  // or with regex:
  //pluginsName 'test-.*\\.jar', true
  // Print some debug [OPTIONAL, default=false]
  debugLog = true
  // Override session
  overrideSession = false
}

// Prints Api version information to see if server is compatible
printApi
```

### Cloudlets tasks

```groovy
createCloudlet {
  // If missing the global one is used [MANDATORY]
  cloudletName = cloudlet
}

startCloudlet {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Fail if already started [OPTIONAL, default=true]
  strictMode = false
  // Build engine and graphQL in parallel [OPTIONAL, default=true]
  parallelBuild = false
}

stopCloudlet {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Fail if already stopped [OPTIONAL, default=true]
  strictMode = true
}

retrieveCloudletInfo {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Output file [MANDATORY]
  outputFile = 'info.json'
}

upgradeCloudlet {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
}

createAdmin {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  cloudletName = 'NewCloudlet1'
}

createMember {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  cloudletName = 'NewCloudlet1'
  username = "user"
  password = "***"
  email = "email@email.it"
  profile = "Member"
  // [OPTIONAL, default false]
  isAdmin = false
}

deleteCloudlet {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  cloudletName = 'NewCloudlet1'
}
```

### Engines tasks

```groovy
deployEngine {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Specify either source cloudlet or source model [MANDATORY]
  //sourceCloudletName = 'NewCloudlet2'
  sourceModelName = 'Model1'
}

uploadEngine {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Input [file, MANDATORY]
  inputFile = 'engine.xml'
}

downloadEngine {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Output [file, MANDATORY]
  outputFile = 'model.xml'
}

deleteEngine {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
}
```

### Database tasks

```groovy
createDatabase {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
}

updateDatabase {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Checks the database before for diff [OPTIONAL, default true]
  checkDatabase = false
}

import static com.fhoster.livebase.gradle.tasks.cloudlet.database.CheckDatabaseTask.FailOn.*

checkDatabase {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  cloudletName = 'NewCloudlet1'
  // Output file [OPTIONAL, if missing a simple log on output will be printed]
  outputFile = 'issues.csv'
  // Fail on given issue severity (HighIssue, AllIssues, Never) [OPTIONAL, default=AllIssues]
  failOn = HighIssue
}

checkEngineDatabaseMismatch {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Output file [OPTIONAL, if missing a log on output will be printed]
  outputFile = 'issues.csv'
  // Fail if there are any issues [OPTIONAL, default false]
  failIfAny = true
}

downloadDatabaseUpdatePlan {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Output [file, MANDATORY]
  outputFile = 'updatePlan.sql'
}

clearDatabase {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  cloudletName = 'NewCloudlet1'
}

downloadDatabase {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Output [file, MANDATORY]
  outputFile = 'db.sql'
}

uploadDatabase {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Input [file, MANDATORY]
  inputFile = 'db.sql'
}
```

### Plugins tasks

```groovy
listPlugins {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Output file [MANDATORY]
  outputFile = 'plugins.txt'
}

downloadEngineSpi {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Output [MANDATORY]
  outputFile = 'plugins/test.jar'
}

uploadPlugins {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Input [single file or directory, MANDATORY, directory with OPTIONAL includes (regex)]
  //inputFile = 'plugins/test.jar'
  inputDirectory = 'plugins/'
  includes = 'test.*\\.jar'
}

downloadPlugins {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // If missing the global one is used [MANDATORY]
  // Can be a regex
  pluginsName 'test.*\\.jar', true
  // Output [directory, MANDATORY]
  outputDirectory = 'plugins/'
}

startPlugins {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // If missing the global one is used [MANDATORY]
  // Can be a regex
  pluginsName 'test.*\\.jar', true
}

stopPlugins {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // If missing the global one is used [MANDATORY]
  // Can be a regex
  pluginsName 'test.*\\.jar', true
}

deletePlugins {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // If missing the global one is used [MANDATORY]
  // Can be a regex
  pluginsName 'test.*\\.jar', true
}

/**
 * Prints state in the format
 * <plugin1Name>: <STATE>
 * <plugin2Name>: <STATE>
 */
printPluginsState {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // If missing the global one is used [MANDATORY]
  // Can be a regex
  pluginsName 'test.*\\.jar', true
  // Output file [MANDATORY]
  outputFile = 'plugins.txt'
}
```

### Models tasks

```groovy
uploadModel {
  // If missing the global one is used [MANDATORY]
  modelName = 'Model1'
  // Model description [MANDATORY]
  modelDescription = 'MyModel'
  // Model comments [MANDATORY]
  modelComments = 'MyModel'
  // Input [file, MANDATORY]
  inputFile = 'model.xml'
}

downloadModel {
  // Model id [MANDATORY, this or name]
  //modelId = id
  // If missing the global one is used [MANDATORY, this or id]
  modelName = 'Model1'
  // Output [file, MANDATORY]
  outputFile = 'model.xml'
}

downloadModelSpi {
  // Model id [MANDATORY, this or name]
  //modelId = id
  // If missing the global one is used [MANDATORY, this or id]
  modelName = 'Model1'
  // Output [file, MANDATORY]
  outputFile = 'modelSpi.jar'
}

deleteModel {
  // Model id [MANDATORY, this or name]
  //modelId = id
  // If missing the global one is used [MANDATORY, this or id]
  modelName = 'Model1'
}
```

### GraphQL tasks
```groovy
enableGraphQL {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  cloudletName = 'NewCloudlet1'

  enablePlugin = false
  // [default is true]
  introspectionEnabled = false
}

rebuildGraphQL {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  cloudletName = 'NewCloudlet1'
}

downloadEngineGraphQL {
  // If missing the global one is used [MANDATORY, this or name]
  //cloudletId = id
  // If missing the global one is used [MANDATORY, this or id]
  cloudletName = cloudlet
  // Output [MANDATORY]
  outputFile = 'engineGraphQL_schema.zip'
}

downloadModelGraphQL {
  // Model id [MANDATORY, this or name]
  //modelId = id
  // If missing the global one is used [MANDATORY, this or id]
  modelName = 'Model1'
  // Output [file, MANDATORY]
  outputFile = 'modelGraphQL_schema.zip'
}
```

### Custom tasks

You can define custom and multiple tasks to interact with multiple cloudlets.

```groovy
import com.fhoster.livebase.gradle.tasks.cloudlet.StartCloudletTask
import com.fhoster.livebase.gradle.tasks.plugins.UploadPluginsTask

livebase {
  // configuration needed as above
}
// Sample task1 that overrides the default one
task _customStart1(type: StartCloudletTask) {
  cloudletName = 'NewCloudlet1'
}

// Sample task2 that overrides the default one
task _customStart2(type: StartCloudletTask) {
  cloudletId 57503
  //cloudletName = 'NewCloudlet2'
}

task _customUploadSpi(type: UploadPluginsTask, dependsOn: downloadEngineSpi) {
  inputFile = downloadEngineSpi.outputFile
}
```

## Changelog

- _2.0.2285_ : Add Enable/disable introspection for GraphQL plugin
- _2.0.1816_ : Fix for model tasks when requiring reservation. Removed `needsLock` property that is now auto-computed
- _2.0.1630_ : Add `createMember` task
- _2.0.1538_ : Add `parallelBuild` option to `startCloudlet`
- _2.0.1438_ : Add `downloadEngineGraphQL` and `downloadModelGraphQL`
- _2.0.1288_ : Add `checkDatabase` option to `updateDatabase` task
- _2.0.1269_ : Add `downloadDatabaseUpdatePlan` and `uploadEngine` tasks
- _2.0.695_  : Add `checkEngineDatabaseMismatch` task
- _2.0.652_  : Add `modelName` global variables and more tests
- _2.0.645_  : Add some more tasks and functional tests
- _2.0.637_  : First public version of the `livebase-gradle-plugin`

