buildscript {
    repositories {
        maven {
            url "https://plugins.gradle.org/m2/"
        }
        mavenCentral()
        gradlePluginPortal()
    }
    dependencies {
        classpath "com.guardsquare:proguard-gradle:7.4.2"
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:1.9.23"
    }
}

plugins {
    id 'idea'
    id 'groovy'
    id 'maven-publish'
    id 'maven'
    id 'signing'
    id 'com.github.hierynomus.license' version '0.15.0'
    id 'io.codearte.nexus-staging' version '0.30.0'
    id 'org.jetbrains.dokka' version "1.9.20"
}
apply plugin: org.jetbrains.kotlin.gradle.plugin.KotlinPluginWrapper

idea.module {
    outputDir file('build/classes/ideaOut/main')
    testOutputDir file('build/classes/ideaOut/test')
    downloadSources = false
}

compileKotlin {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

signing {
    sign configurations.archives
}

apply from: "$rootDir/gradle/versioning.gradle"
apply from: "$rootDir/gradle/environmentsConfig.gradle"

def nexus_user = hasProperty('nexus_user') ? getProperty('nexus_user') : ext.hasProperty('nexus_user') ?: ''
def nexus_password = hasProperty('nexus_password') ? getProperty('nexus_password') : ext.hasProperty('nexus_password') ?: ''

if (nexus_user.isEmpty()) {
    throw new GradleException("nexus_user properties is not set. Please use -Pnexus_user=<value> or specify it in local.properties")
}
if (nexus_password.isEmpty()) {
    throw new GradleException("nexus_password properties is not set. Please use -Pnexus_password=<value> or specify it in local.properties")
}

group 'com.fhoster.livebase'
def dependencyVer = getVersion()
archivesBaseName = 'livebase-gradle-plugin'
description 'Gradle plugin to interact with Livebase cloudlets'

repositories() {
    mavenCentral()
    maven {
        credentials {
            username nexus_user
            password nexus_password
        }
        url repo.url
    }
}

license {
    header file('codequality/HEADER')
    includes(["**/*.kt"])
    ext.copyrightOwner = "Fhoster srl"
    ext.year = Calendar.getInstance().get(Calendar.YEAR)
}
licenseMain.enabled = false
licenseTest.enabled = false

jar.dependsOn 'licenseFormatTest', 'licenseFormat'

configurations {
    plugin
    proguardLibs
}

dependencies {
    implementation gradleApi()
    implementation("livebase.dashboardServantApi:connectionApi:${dependencyVer}")
    implementation "org.jetbrains.kotlin:kotlin-stdlib"
    implementation "com.cedarsoftware:json-io:4.24.0"
    implementation "org.apache.httpcomponents:httpclient:4.5.9"
    implementation "org.apache.httpcomponents:httpmime:4.5.9"

    testImplementation "junit:junit:4.12"
    testImplementation gradleTestKit()

    plugin("livebase.api:apiDatabase:${dependencyVer}")
    plugin("livebase.api:apiHosting:${dependencyVer}")
    plugin("livebase.dashboardServantApi:connectionApi:${dependencyVer}") {
        transitive = false
    }
    plugin("livebase.dashboardServantApi:servletInterface:${dependencyVer}") {
        transitive = false
    }
    proguardLibs gradleApi()
    proguardLibs localGroovy()
    proguardLibs "com.cedarsoftware:json-io:4.24.0"
    proguardLibs "org.apache.httpcomponents:httpclient:4.5.9"
    proguardLibs "org.apache.httpcomponents:httpmime:4.5.9"
    proguardLibs "org.jetbrains.kotlin:kotlin-stdlib-jdk8"
}

jar {
    from {
        configurations.plugin.collect {
            it.isDirectory() ? it : zipTree(it)
        }
    }
}

task sourcesJar(type: Jar, dependsOn: classes) {
    classifier = 'sources'
    from sourceSets.main.allSource
}

task javadocJar(type: Jar, dependsOn: dokkaJavadoc) {
    classifier = 'javadoc'
    from dokkaJavadoc
}

def tmpDir = "$buildDir/proguard/"
def javaHome = System.getProperty('java.home')
def pluginJar = "$buildDir/libs/${archivesBaseName}.jar"

import proguard.gradle.ProGuardTask

task obfuscate(type: ProGuardTask, dependsOn: jar) {
    doFirst {
        file(tmpDir).mkdirs()
    }
    injars jar.outputs.files.singleFile
    outjars "$pluginJar"
    inputs.file jar.outputs.files.singleFile
    outputs.file "$pluginJar"
    libraryjars "$javaHome/lib/rt.jar"
    libraryjars "$javaHome/lib/jce.jar"
    libraryjars "$javaHome/lib/jsse.jar"
    libraryjars configurations.proguardLibs.files
    target "1.8"
    configuration 'proguard.pro'
    printmapping "$tmpDir/mapping.proguard"
    printseeds "$tmpDir/seeds.proguard"
    printusage "$tmpDir/usage.proguard"
}

artifacts {
    archives sourcesJar, javadocJar
}

def pluginArtifact = artifacts.add('archives', file(pluginJar)) {
    type 'jar'
    builtBy 'obfuscate'
}

test.dependsOn jar

publishing {
    publications {
        "$name"(MavenPublication) {
            groupId group
            artifactId archivesBaseName

            artifact pluginArtifact
            artifact sourcesJar { classifier "sources" }
            artifact javadocJar { classifier "javadoc" }
        }
    }

    repositories {
        maven {
            url project.version.toString().contains('SNAPSHOT') ? repo.snapshotUrl : repo.releaseUrl
            credentials {
                username nexus_user
                password nexus_password
            }
        }
    }
}

uploadArchives {
    repositories {
        mavenDeployer {
            beforeDeployment { MavenDeployment deployment -> signing.signPom(deployment) }

            repository(url: "https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/") {
                authentication(userName: ossrhUsername, password: ossrhPassword)
            }

            snapshotRepository(url: "https://s01.oss.sonatype.org/content/repositories/snapshots/") {
                authentication(userName: ossrhUsername, password: ossrhPassword)
            }

            pom.project {
                name project.name
                packaging 'jar'
                // optionally artifactId can be defined here
                description project.description
                url 'https://bitbucket.org/fhostercom/livebase-gradle-plugin/'

                scm {
                    connection 'scm:git:https://bitbucket.org/fhostercom/livebase-gradle-plugin/src/master'
                    developerConnection 'scm:git:https://bitbucket.org/fhostercom/livebase-gradle-plugin/src/master/'
                    url 'https://bitbucket.org/fhostercom/livebase-gradle-plugin/'
                }

                licenses {
                    license {
                        name 'The Apache License, Version 2.0'
                        url 'http://www.apache.org/licenses/LICENSE-2.0.txt'
                    }
                }

                developers {
                    developer {
                        id 'fhoster'
                        name 'Fhoster Srl'
                        email 'support@fhoster.com'
                    }
                }
            }
            pom.whenConfigured {
                p ->
                    p.dependencies = p.dependencies.findAll {
                        d -> !d.groupId.startsWith('livebase')
                    }
            }
        }
    }
}

nexusStaging {
    serverUrl = "https://s01.oss.sonatype.org/service/local/"
    packageGroup = "com.fhoster"
}

def getVersion() {
    if (hasProperty('lbVer'))
        return getProperty('lbVer')
    return ext.hasProperty('lbVer') ?: "[2.0.2593, )"
}

