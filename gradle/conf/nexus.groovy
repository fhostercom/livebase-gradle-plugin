environments {
    linodeNexus {
        baseUrl = "https://nexus3.fhoster.com/repository"
        releaseUrl = "$baseUrl/livebase-gradle-plugin/"
        snapshotUrl = "$baseUrl/livebase-gradle-plugin-snapshot/"
        url = "$baseUrl/maven-public/"
    }    
}
