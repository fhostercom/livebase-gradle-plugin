/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle

import com.fhoster.livebase.gradle.tasks.LivebaseTask
import com.fhoster.livebase.gradle.tasks.PrintApiTask
import com.fhoster.livebase.gradle.tasks.cloudlet.*
import com.fhoster.livebase.gradle.tasks.cloudlet.database.*
import com.fhoster.livebase.gradle.tasks.cloudlet.engine.DeleteEngineTask
import com.fhoster.livebase.gradle.tasks.cloudlet.engine.DeployEngineTask
import com.fhoster.livebase.gradle.tasks.cloudlet.engine.DownloadEngineTask
import com.fhoster.livebase.gradle.tasks.cloudlet.engine.UploadEngineTask
import com.fhoster.livebase.gradle.tasks.model.*
import com.fhoster.livebase.gradle.tasks.plugins.*
import org.gradle.api.Plugin
import org.gradle.api.Project
import proguard.marker.ObfuscateKeepMembers

open class LivebasePlugin : Plugin<Project>, ObfuscateKeepMembers {

    override fun apply(project: Project) {
        val ext = project.extensions.create(livebaseExtensionName, LivebasePluginExtension::class.java)

        project.tasks.create("printApi", PrintApiTask::class.java)

        //#region Cloudlet tasks
        project.tasks.create("createCloudlet", CreateCloudletTask::class.java)
        project.tasks.create("startCloudlet", StartCloudletTask::class.java)
        project.tasks.create("stopCloudlet", StopCloudletTask::class.java)
        project.tasks.create("retrieveCloudletInfo", RetrieveCloudletInfoTask::class.java)
        project.tasks.create("upgradeCloudlet", UpgradeCloudletTask::class.java)
        project.tasks.create("enableGraphQL", EnableGraphQLTask::class.java)
        project.tasks.create("rebuildGraphQL", RebuildGraphQLTask::class.java)
        project.tasks.create("deleteCloudlet", DeleteCloudletTask::class.java)
        //#region Engine tasks
        project.tasks.create("deployEngine", DeployEngineTask::class.java)
        project.tasks.create("deleteEngine", DeleteEngineTask::class.java)
        project.tasks.create("downloadEngine", DownloadEngineTask::class.java)
        project.tasks.create("uploadEngine", UploadEngineTask::class.java)
        //#endregion
        //#region Database tasks
        project.tasks.create("createDatabase", CreateDatabaseTask::class.java)
        project.tasks.create("updateDatabase", UpdateDatabaseTask::class.java)
        project.tasks.create("checkDatabase", CheckDatabaseTask::class.java)
        project.tasks.create("checkEngineDatabaseMismatch", CheckEngineDatabaseMismatchTask::class.java)
        project.tasks.create("clearDatabase", ClearDatabaseTask::class.java)
        project.tasks.create("downloadDatabase", DownloadDatabaseTask::class.java)
        project.tasks.create("downloadDatabaseUpdatePlan", DownloadDatabaseUpdatePlanTask::class.java)
        project.tasks.create("uploadDatabase", UploadDatabaseTask::class.java)
        project.tasks.create("createAdmin", CreateAdminTask::class.java)
        project.tasks.create("createMember", CreateMemberTask::class.java)
        //#endregion
        //#endregion

        //#region Plugin tasks
        project.tasks.create("listPlugins", ListPluginsTask::class.java)
        project.tasks.create("downloadEngineSpi", DownloadEngineSpiTask::class.java)
        project.tasks.create("uploadPlugins", UploadPluginsTask::class.java)
        project.tasks.create("downloadPlugins", DownloadPluginsTask::class.java)
        project.tasks.create("startPlugins", StartPluginsTask::class.java)
        project.tasks.create("stopPlugins", StopPluginsTask::class.java)
        project.tasks.create("deletePlugins", DeletePluginsTask::class.java)
        project.tasks.create("printPluginsState", PrintPluginsStateTask::class.java)
        project.tasks.create("downloadEngineGraphQL", DownloadEngineGraphQLTask::class.java)
        //#endregion

        //#region Model tasks
        project.tasks.create("uploadModel", UploadModelTask::class.java)
        project.tasks.create("downloadModel", DownloadModelTask::class.java)
        project.tasks.create("downloadModelSpi", DownloadModelSpiTask::class.java)
        project.tasks.create("deleteModel", DeleteModelTask::class.java)
        project.tasks.create("downloadModelGraphQL", DownloadModelGraphQLTask::class.java)
        //#endregion

        project.afterEvaluate { p ->
            p.tasks.withType(LivebaseTask::class.java) { t ->
                t.configure(ext)
            }
        }

        project.gradle.buildFinished {
            ext.disconnect()
        }
    }
}
