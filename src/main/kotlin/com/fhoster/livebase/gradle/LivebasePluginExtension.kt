/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle

import com.fhoster.livebase.dashboard.actions.LivebaseConnection
import com.fhoster.livebase.dashboard.net.utils.DashboardServantConfiguration.fromServantUrl
import com.fhoster.livebase.gradle.tasks.cloudlet.CloudletResolver
import com.fhoster.livebase.gradle.tasks.model.ModelResolver
import com.fhoster.livebase.gradle.tasks.plugins.PluginsResolver
import org.gradle.api.Project
import org.gradle.api.logging.LogLevel
import proguard.marker.ObfuscateKeepMembers

const val livebaseExtensionName = "livebase"

open class LivebasePluginExtension : ObfuscateKeepMembers {

    init {
        production()
    }

    fun production() {
        servantUrl = "https://fs2.fhoster.com/dashboardWebapp/"
    }

    fun validation() {
        servantUrl = "https://validation.fhoster.com/dashboardWebapp/"
    }

    fun localhost() {
        servantUrl = "http://localhost:8080/dashboardWebapp/"
    }

    lateinit var servantUrl: String
    lateinit var username: String
    lateinit var password: String

    var overrideSession = false
    var debugLog = false
    var needsLock = false

    internal val cloudletResolver = CloudletResolver()
    internal val modelResolver = ModelResolver()
    internal val pluginsResolver = PluginsResolver()

    fun setCloudletId(cloudletId: Long) {
        cloudletResolver.cloudletId = cloudletId
    }

    fun setCloudletName(cloudletName: String) {
        cloudletResolver.cloudletName = cloudletName
    }

    fun setModelName(modelName: String) {
        modelResolver.modelName = modelName
    }

    fun setPluginsName(pluginsName: String) {
        // Can't use default value in Gradle
        pluginsName(pluginsName, false)
    }

    fun pluginsName(pluginsName: String, isRegex: Boolean) {
        pluginsResolver.pluginsName = pluginsName
        pluginsResolver.isRegex = isRegex
    }

    internal fun connect() {
        connection.printLog = debugLog
        val server = fromServantUrl(servantUrl)
        connection.connectToServer(server, username, password, overrideSession)
    }

    internal fun disconnect() {
        if (connected)
            connection.closeConnection()
        connected = false
    }

    companion object INSTANCE {

        private val connection = LivebaseConnection()
        private var connected = false

        fun getConnection(prj: Project): LivebaseConnection {
            if (!connected) {
                val ext =
                    prj.extensions.findByName(livebaseExtensionName) as LivebasePluginExtension
                if (ext.debugLog)
                    prj.logger.log(LogLevel.INFO, "Connecting to ${ext.servantUrl}")
                ext.connect()
                connected = true
            }
            return connection
        }
    }
}
