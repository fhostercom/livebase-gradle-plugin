/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks

import com.fhoster.livebase.dashboard.actions.DashboardAction
import com.fhoster.livebase.dashboard.actions.LivebaseConnection
import com.fhoster.livebase.dashboard.actions.ServantAction
import com.fhoster.livebase.gradle.LivebasePluginExtension
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import proguard.marker.ObfuscateKeepMembers

abstract class LivebaseTask : DefaultTask(), ObfuscateKeepMembers {

    init {
        group = "Livebase"
    }

    @Input
    var debugLog: Boolean? = null

    val needsLock: Boolean
        get() = dashboardAction.data.locks.areLocksNeeded()

    open fun configure(ext: LivebasePluginExtension) {
        if (debugLog == null)
            debugLog = ext.debugLog
    }

    @get:Internal
    protected val connection: LivebaseConnection
        get() = LivebasePluginExtension.getConnection(project)

    @get:Internal
    protected val dashboardAction: DashboardAction by lazy {
        connection.newDashboardAction()
    }

    @get:Internal
    protected open val action: ServantAction
        get() = throw GradleException("Not implemented")

    protected fun checkNotNullSingleParam(vararg param: Any?): Boolean {
        return param.count { p -> p != null } == 1
    }
}
