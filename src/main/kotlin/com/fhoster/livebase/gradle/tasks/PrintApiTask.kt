/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks

import com.fhoster.livebase.dashboard.servletInterface.ServletInterface
import com.fhoster.livebase.gradle.LivebasePluginExtension
import com.fhoster.livebase.gradle.livebaseExtensionName
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.TaskAction
import org.plibrary.xml.XmlDocument
import java.net.URL

open class PrintApiTask : LivebaseTask() {

    init {
        description = "Prints API compatibility information"
    }

    @TaskAction
    open fun printApi() {
        val server = getVersionFromServer()
        val plugin = ServletInterface.SERVLET_INTERFACE_VERSION
        if (server != plugin)
            logger.log(LogLevel.ERROR, "ERROR: Api versions are different")
        println("ServerApiVersion: $server")
        println("PluginApiVersion: $plugin")
    }

    /**
     * Since we cannot access Updater project we stay with version 4 of updater,
     * when this version will no longer be supported this should be updated
     */
    private fun getVersionFromServer(): Long {
        val servantUrl =
            (project.extensions.findByName(livebaseExtensionName) as LivebasePluginExtension).servantUrl
        val url = "${servantUrl}DashboardJnlpForUpdateServlet?updaterVersion=4"
        val c = URL(url).openConnection()
        c.connectTimeout = 3000
        c.getInputStream().use {
            val doc = XmlDocument(it)
            val node = doc.rootNode.getNodeByPath("app", "version")
            return node.getAttribute("servletInterface", Long::class.java, true)
        }
    }
}
