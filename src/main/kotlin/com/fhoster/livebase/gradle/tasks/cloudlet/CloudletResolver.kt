/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.cloudlet

import com.fhoster.livebase.dashboard.actions.CloudletAction
import com.fhoster.livebase.dashboard.actions.LivebaseConnection
import com.fhoster.livebase.dashboard.actions.ManagePluginAction
import org.gradle.api.GradleException
import proguard.marker.ObfuscateKeepMembers

class CloudletResolver : ObfuscateKeepMembers {

    var cloudletId: Long? = null
    var cloudletName: String? = null

    val cloudletIdentifier: String
        get() = cloudletName ?: cloudletId.toString()

    private val exception: GradleException
        get() = GradleException("No cloudlet specified (neither id nor name)")

    fun mergeWith(otherResolver: CloudletResolver) {
        if (cloudletId == null && cloudletName == null) {
            cloudletId = otherResolver.cloudletId
            cloudletName = otherResolver.cloudletName
        }
    }

    fun initCloudletAction(con: LivebaseConnection): CloudletAction {
        return when {
            cloudletId != null -> con.newCloudletAction(cloudletId!!)
            cloudletName != null -> con.newCloudletAction(cloudletName!!)
            else -> throw exception
        }
    }

    fun initPluginAction(con: LivebaseConnection): ManagePluginAction {
        return when {
            cloudletId != null -> con.newPluginAction(cloudletId!!)
            cloudletName != null -> con.newPluginAction(cloudletName!!)
            else -> throw exception
        }
    }

    fun changeCloudlet(a: CloudletAction) {
        when {
            cloudletId != null -> a.changeCloudlet(cloudletId!!)
            cloudletName != null -> a.changeCloudlet(cloudletName!!)
            else -> throw exception
        }
    }
}
