/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.cloudlet

import com.fhoster.livebase.dashboard.actions.CloudletAction
import com.fhoster.livebase.gradle.LivebasePluginExtension
import com.fhoster.livebase.gradle.tasks.LivebaseTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional

abstract class CloudletTask : LivebaseTask() {

    init {
        group = "Livebase Cloudlet"
    }

    @get:Internal
    protected val cloudletResolver = CloudletResolver()

    @Input
    @Optional
    fun setCloudletId(cloudletId: Long) {
        cloudletResolver.cloudletId = cloudletId
    }

    @Input
    @Optional
    fun setCloudletName(cloudletName: String) {
        cloudletResolver.cloudletName = cloudletName
    }

    @get:Internal
    val cloudletIdentifier: String
        get() = cloudletResolver.cloudletIdentifier

    @get:Internal
    override val action: CloudletAction by lazy {
        cloudletResolver.initCloudletAction(connection)
    }

    override fun configure(ext: LivebasePluginExtension) {
        super.configure(ext)
        cloudletResolver.mergeWith(ext.cloudletResolver)
    }

    protected fun onCloudlet(op: () -> Unit) {
        cloudletResolver.changeCloudlet(action)
        if (needsLock)
            action.executeWithReservation<Exception> {
                op()
            }
        else
            op()
    }
}
