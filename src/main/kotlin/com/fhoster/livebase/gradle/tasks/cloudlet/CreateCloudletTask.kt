/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.cloudlet

import com.fhoster.livebase.gradle.LivebasePluginExtension
import com.fhoster.livebase.gradle.tasks.LivebaseTask
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction

open class CreateCloudletTask : LivebaseTask() {

    init {
        group = "Livebase Cloudlet"
        description = "Creates the cloudlet"
    }

    @get:Internal
    private val cloudletResolver = CloudletResolver()

    @Input
    @Optional
    fun setCloudletName(cloudletName: String) {
        cloudletResolver.cloudletName = cloudletName
    }

    override fun configure(ext: LivebasePluginExtension) {
        super.configure(ext)
        cloudletResolver.mergeWith(ext.cloudletResolver)
    }

    @TaskAction
    open fun createCloudlet() {
        logger.log(LogLevel.INFO, "Creating a new cloudlet")
        val withNonGraphQL: Boolean = dashboardAction.data.account.canViewNonGraphQL
        val c = dashboardAction.createCloudlet(!withNonGraphQL)
        val ca = connection.newCloudletAction(c.id)

        logger.log(LogLevel.INFO, "Renaming cloudlet ${c.name} to ${cloudletResolver.cloudletName}")
        if (needsLock)
            ca.executeWithReservation<Exception> {
                ca.renameCloudlet(cloudletResolver.cloudletName)
            }
        else
            ca.renameCloudlet(cloudletResolver.cloudletName)

    }
}
