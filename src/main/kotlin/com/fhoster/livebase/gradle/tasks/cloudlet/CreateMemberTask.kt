/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.cloudlet

import com.fhoster.livebase.dashboard.servletInterface.cloudlet.DashboardCloudletNewMemberDescriptor
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction

open class CreateMemberTask : CloudletTask() {

    init {
        description = "Creates a cloudlet member"
    }

    @Input
    lateinit var username: String

    @Input
    lateinit var password: String

    @Input
    lateinit var email: String

    @Input
    lateinit var profile: String

    @Input
    @Optional
    @get:JvmName("isAdmin")
    var isAdmin: Boolean = false

    @TaskAction
    open fun createMember() {
        onCloudlet {
            logger.log(LogLevel.INFO, "Creating member $username on $cloudletIdentifier")
            val d = DashboardCloudletNewMemberDescriptor(username, password, email, profile)
            d.isAdministrator = isAdmin
            action.createMember(d)
        }
    }
}
