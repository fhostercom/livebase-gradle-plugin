/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.cloudlet

import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

open class RetrieveCloudletInfoTask : CloudletTask() {

    init {
        description = "Prints some useful cloudlet information"
        outputs.upToDateWhen { false }
    }

    @OutputFile
    lateinit var outputFile: Any

    @TaskAction
    open fun retrieveCloudletInfo() {
        action.refresh()
        val d = action.descriptor
        project.file(outputFile).writeText("""
            |{
            |    "id": ${action.cloudletId},
            |    "url": "${d.url}",
            |    "builder-version": "${d.builderVersion ?: "-"}"
            |}
            |
        """.trimMargin("|"), charset = Charsets.UTF_8)
    }
}
