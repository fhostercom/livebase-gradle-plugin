/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.cloudlet.database

import com.fhoster.livebase.common.databaseUpdater.SeverityLevel
import com.fhoster.livebase.dashboard.servletInterface.DashboardDatabaseCompatibilityIssue
import com.fhoster.livebase.gradle.tasks.cloudlet.CloudletTask
import org.gradle.api.GradleException
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import proguard.marker.ObfuscateKeepMembers
import java.io.File

open class CheckDatabaseTask : CloudletTask() {

    init {
        description = "Checks database compatibility issues"
        outputs.upToDateWhen { false }
    }

    @OutputFile
    @Optional
    var outputFile: Any? = null

    @Input
    @Optional
    var failOn: FailOn = FailOn.AllIssues

    @TaskAction
    open fun checkDatabase() {
        onCloudlet {
            logger.log(LogLevel.INFO, "Checking database of $cloudletIdentifier")
            val report = action.checkForDatabaseIssues()
            val orderedIssues = report.compatibilityIssues.sortedBy { it.severityLevel }
            val maxIssue = orderedIssues.firstOrNull()

            if (outputFile != null) {
                val file = project.file(outputFile!!)
                if (orderedIssues.any())
                    writeIssuesToFile(orderedIssues, file)
                else
                    file.delete()
            } else {
                if (orderedIssues.any())
                    logger.log(LogLevel.ERROR, issueMessage(maxIssue))
            }

            if (failOn.shouldFail(maxIssue?.severityLevel))
                throw GradleException(issueMessage(maxIssue))
        }
    }

    private fun issueMessage(maxIssue: DashboardDatabaseCompatibilityIssue?): String =
            "Database has at least an issue: '${maxIssue.toString().cleanHtml()}', " +
                    if (outputFile != null) "more details on output file" else "no output file specified"

    private fun writeIssuesToFile(issues: Iterable<DashboardDatabaseCompatibilityIssue>, file: File) {
        val b = StringBuilder()
        b.append("Severity,Category,ModelReference,RequiredAction,Description\n")
        issues.forEach { i ->
            b.append("${i.severityLevel.name},${i.issueCategory},${i.modelReference}," +
                    "${i.requiredAction?.cleanHtml() ?: ""},${i.issueTypeDescription.cleanHtml()}\n")
        }
        file.writeText(b.toString())
    }

    private fun String.cleanHtml(): String {
        return replace("</?[b|i]>".toRegex(), "'")
    }

    enum class FailOn : ObfuscateKeepMembers {

        HighIssue,
        AllIssues,
        Never;

        internal fun shouldFail(level: SeverityLevel?): Boolean {
            return when {
                this == Never -> false
                this == AllIssues -> level != null
                this == HighIssue -> level == SeverityLevel.HIGH
                else -> true
            }
        }
    }
}
