/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.cloudlet.database

import com.fhoster.livebase.dashboard.servletInterface.DashboardDatabaseStructureDiff
import com.fhoster.livebase.dashboard.servletInterface.request.CheckEngineDatabaseStructureDiffRequest
import com.fhoster.livebase.dashboard.servletInterface.response.CheckEngineDatabaseStructureDiffResponse
import com.fhoster.livebase.gradle.tasks.cloudlet.CloudletTask
import org.gradle.api.GradleException
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import java.io.File

open class CheckEngineDatabaseMismatchTask : CloudletTask() {

    init {
        description = "Checks engine database mismatch"
        outputs.upToDateWhen { false }
    }

    @OutputFile
    @Optional
    var outputFile: Any? = null

    @Input
    @Optional
    var failIfAny: Boolean = false

    @TaskAction
    open fun checkEngineDatabaseMismatch() {
        onCloudlet {
            logger.log(LogLevel.INFO, "Checking engine/database mismatch of $cloudletIdentifier")
            val report = action.checkEngineDatabaseMismatch()
            val issues = report.diff
            if (outputFile != null) {
                val file = project.file(outputFile!!)
                if (!report.match())
                    writeIssuesToFile(issues, file)
                else
                    file.delete()
            } else {
                if (!report.match())
                    logger.log(LogLevel.ERROR, issueMessage(issues))
            }
            if (!report.match() && failIfAny)
                throw GradleException(issueMessage(issues))
        }
    }

    private fun issueMessage(issues: Iterable<DashboardDatabaseStructureDiff>): String {
        val b = StringBuilder()
        b.append("DiffType,DataType,Name\n")
        issues.forEach { i ->
            b.append("${i.diffType.name},${i.dataType.name},${i.name}\n")
        }
        return b.toString()
    }

    private fun writeIssuesToFile(issues: Iterable<DashboardDatabaseStructureDiff>, file: File) {
        file.writeText(issueMessage(issues))
    }
}
