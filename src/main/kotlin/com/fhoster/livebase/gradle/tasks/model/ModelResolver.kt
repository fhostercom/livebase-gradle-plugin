/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.model

import com.fhoster.livebase.dashboard.actions.LivebaseConnection
import com.fhoster.livebase.dashboard.actions.ModelAction
import org.gradle.api.GradleException
import proguard.marker.ObfuscateKeepMembers

class ModelResolver : ObfuscateKeepMembers {

    var modelId: Long? = null
    var modelName: String? = null

    val modelIdentifier: String
        get() = modelName ?: modelId.toString()

    private val exception: GradleException
        get() = GradleException("No model specified (neither id nor name)")

    fun mergeWith(otherResolver: ModelResolver) {
        if (modelId == null && modelName == null) {
            modelId = otherResolver.modelId
            modelName = otherResolver.modelName
        }
    }

    fun initModelAction(con: LivebaseConnection): ModelAction {
        return when {
            modelId != null -> con.newModelAction(modelId!!)
            modelName != null -> con.newModelAction(modelName!!)
            else -> throw exception
        }
    }

    fun changeModel(a: ModelAction) {
        when {
            modelId != null -> a.changeModel(modelId!!)
            modelName != null -> a.changeModel(modelName!!)
            else -> throw exception
        }
    }
}
