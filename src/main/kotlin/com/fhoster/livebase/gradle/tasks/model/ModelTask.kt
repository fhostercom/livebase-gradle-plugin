/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.model

import com.fhoster.livebase.dashboard.actions.ModelAction
import com.fhoster.livebase.gradle.LivebasePluginExtension
import com.fhoster.livebase.gradle.tasks.LivebaseTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional

abstract class ModelTask : LivebaseTask() {

    init {
        group = "Livebase Model"
    }

    @get:Internal
    private val modelResolver = ModelResolver()

    @Input
    @Optional
    fun setModelId(modelId: Long) {
        modelResolver.modelId = modelId
    }

    @Input
    @Optional
    fun setModelName(modelName: String) {
        modelResolver.modelName = modelName
    }

    @get:Internal
    val modelIdentifier: String
        get() = modelResolver.modelIdentifier

    @get:Internal
    override val action: ModelAction by lazy {
        modelResolver.initModelAction(connection)
    }

    override fun configure(ext: LivebasePluginExtension) {
        super.configure(ext)
        modelResolver.mergeWith(ext.modelResolver)
    }

    protected fun onModel(op: () -> Unit) {
        modelResolver.changeModel(action)
        if (needsLock)
            action.executeWithReservation<Exception> {
                op()
            }
        else
            op()
    }
}
