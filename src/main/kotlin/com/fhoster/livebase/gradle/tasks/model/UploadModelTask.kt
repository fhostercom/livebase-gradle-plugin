/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.model

import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.TaskAction

open class UploadModelTask : ModelTask() {

    init {
        description = "Uploads a model into library"
    }

    @Input
    lateinit var modelComments: String

    @Input
    lateinit var modelDescription: String

    @InputFile
    lateinit var inputFile: Any

    @TaskAction
    open fun uploadModel() {
        val file = project.file(inputFile)
        logger.log(LogLevel.INFO, "Uploading model $modelIdentifier from $file")
        val xml = file.readText()
        if (dashboardAction.data.getModelWithName(modelIdentifier) != null) {
            onModel {
                dashboardAction.uploadModel(modelIdentifier, modelDescription, modelComments, xml)
            }
        } else {
            dashboardAction.uploadModel(modelIdentifier, modelDescription, modelComments, xml)
        }
    }
}
