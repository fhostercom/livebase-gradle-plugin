/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.plugins

import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import java.io.File

open class DownloadPluginsTask : MultiplePluginsTask() {

    init {
        description = "Downloads the plugins"
    }

    @OutputDirectory
    lateinit var outputDirectory: Any

    @TaskAction
    open fun downloadPlugins() {
        onCloudlet {
            val dir = project.file(outputDirectory)
            plugins.forEach { p ->
                val jar = File(dir, p)
                logger.log(LogLevel.INFO, "Downloading $p to $jar")
                action.downloadPlugin(p, jar)
            }
        }
    }
}
