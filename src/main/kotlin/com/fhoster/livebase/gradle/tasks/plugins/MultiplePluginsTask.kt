/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.plugins

import com.fhoster.livebase.gradle.LivebasePluginExtension
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal

abstract class MultiplePluginsTask : PluginTask() {

    @get:Internal
    private val pluginsResolver = PluginsResolver()

    @Input
    fun pluginsName(pluginsName: String) {
        // Can't use default value in Gradle
        pluginsName(pluginsName, false)
    }

    @Input
    fun pluginsName(pluginsName: String, isRegex: Boolean) {
        pluginsResolver.pluginsName = pluginsName
        pluginsResolver.isRegex = isRegex
    }

    override fun configure(ext: LivebasePluginExtension) {
        super.configure(ext)
        pluginsResolver.mergeWith(ext.pluginsResolver)
    }

    protected val plugins: List<String>
        get() = pluginsResolver.applyToPluginsList(action.listPlugins())
}
