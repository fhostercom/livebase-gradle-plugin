/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.plugins

import org.gradle.api.GradleException
import proguard.marker.ObfuscateSerialize

class PluginsResolver : ObfuscateSerialize {

    var pluginsName: String? = null
    var isRegex: Boolean = false

    private val exception: GradleException
        get() = GradleException("No plugin specified")

    fun mergeWith(otherResolver: PluginsResolver) {
        if (pluginsName == null) {
            pluginsName = otherResolver.pluginsName
            isRegex = otherResolver.isRegex
        }
    }

    fun applyToPluginsList(plugins: Array<String>): List<String> {
        if (pluginsName == null)
            throw exception
        return if (isRegex) {
            val regex = pluginsName!!.toRegex()
            plugins.filter { s ->
                regex.containsMatchIn(s)
            }
        } else {
            return plugins.filter { s ->
                s == pluginsName
            }
        }
    }
}
