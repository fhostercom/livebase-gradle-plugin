/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle.tasks.plugins

import org.gradle.api.GradleException
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.*
import java.io.File

open class UploadPluginsTask : PluginTask() {

    init {
        description = "Uploads the plugins"
    }

    @InputFile
    @Optional
    var inputFile: Any? = null

    @InputDirectory
    @Optional
    var inputDirectory: Any? = null

    @Input
    @Optional
    var includes: String? = null

    @TaskAction
    open fun uploadPlugins() {
        if (!checkNotNullSingleParam(inputFile, inputDirectory))
            throw GradleException("A single input source must be given")
        onCloudlet {
            listFiles().forEach { f ->
                logger.log(LogLevel.INFO, "Uploading $f")
                action.uploadPlugin(f)
            }
        }
    }

    private fun listFiles(): Array<File> {
        return if (inputFile != null) {
            arrayOf(project.file(inputFile!!))
        } else {
            val checker: (f: File) -> Boolean =
                    if (includes != null) {
                        val regex = includes!!.toRegex()
                        ({ f -> regex.containsMatchIn(f.name) })
                    } else {
                        { f -> f.extension == "jar" }
                    }
            project.file(inputDirectory!!).listFiles { f ->
                checker(f)
            } ?: arrayOf()
        }
    }
}
