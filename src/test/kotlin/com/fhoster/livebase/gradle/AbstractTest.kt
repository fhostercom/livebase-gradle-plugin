/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle

import org.gradle.internal.impldep.com.google.common.io.Files
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.After
import org.junit.Before
import org.plib.javaClassPath
import org.plib.pathSeparator
import org.plibrary.Environment
import java.io.File

abstract class AbstractTest {

    lateinit var testProjectDir: File
    lateinit var settingsFile: File
    lateinit var buildFile: File

    @Before
    fun setup() {
        testProjectDir = Files.createTempDir()
        println(">> Testing project $testProjectDir <<")
        settingsFile = File(testProjectDir, "settings.gradle")
        settingsFile.writeText("rootProject.name = 'test'")
        buildFile = File(testProjectDir, "build.gradle")
        File("build/libs").copyRecursively(File(testProjectDir, "libs/"), overwrite = true)
    }

    private fun build(arg: String): BuildResult {
        return GradleRunner.create()
            .withProjectDir(testProjectDir)
            .withArguments(arg, "--stacktrace")
            .build()
    }

    abstract val cloudletName: String
    abstract val modelName: String

    private val gradleClassPath: String by lazy {
        javaClassPath.split(pathSeparator).filter { p ->
            File(p).isFile
        }.joinToString(separator = "\n") { p ->
            "classpath files('$p')"
        }
    }

    protected fun testTask(taskCode: String, taskName: String = taskCode): BuildResult {
        if (taskName.contains('{'))
            throw IllegalArgumentException("Task name must be a simple string")
        val taskText = """
            $buildConfiguration
            $taskCode
            """.trimIndent()
        buildFile.writeText(taskText)
        return build(taskName)
    }

    private val buildConfiguration: String by lazy {
        """|buildscript {
           |    dependencies {
           |        classpath fileTree(dir: 'libs', include: ['*.jar'])
           |        $gradleClassPath
           |    }
           | }
           | apply plugin: 'com.fhoster.livebase'
           | livebase {
           |    $server()
           |    username = '$username'
           |    password = '$password'
           |    overrideSession = true
           |    debugLog = true
           |    cloudletName = '$cloudletName'
           |    modelName = '$modelName'
           |}
        """.trimMargin()
    }


    @After
    fun cleanup() {
        testProjectDir.deleteRecursively()
    }

    companion object {
        val server: String
            get() = System.getenv("lb_server")
        val username: String
            get() = System.getenv("lb_username")
        val password: String
            get() = System.getenv("lb_password")
        val canRunRequestApi: Boolean
            get() = true //server != "localhost"
    }
}
