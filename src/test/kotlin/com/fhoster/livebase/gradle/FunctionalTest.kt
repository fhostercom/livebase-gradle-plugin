/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle

import com.fhoster.livebase.dashboard.actions.LivebaseConnection
import com.fhoster.livebase.dashboard.net.utils.DashboardServantConfiguration
import com.fhoster.livebase.dashboard.servletInterface.ServletInterface
import com.fhoster.livebase.dashboard.servletInterface.request.ResetDashboardCloudletsRequest
import com.fhoster.livebase.dashboard.servletInterface.request.ResetModelLibraryRequest
import com.fhoster.livebase.dashboard.servletInterface.response.ServantResponse
import junit.framework.Assert.assertTrue
import org.junit.Assume
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import java.io.File
import java.nio.file.Files

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class FunctionalTest : AbstractTest() {

    override val cloudletName: String
        get() = "Test"
    override val modelName: String
        get() = "Model1"

    @Test
    fun test_00_PrintApi() {
        Assume.assumeTrue(canRunRequestApi)
        val res = testTask("printApi")
        assertTrue("ServerAPI ${ServletInterface.SERVLET_INTERFACE_VERSION} not found in:\n${res.output}",
                res.output.contains("ServerApiVersion: ${ServletInterface.SERVLET_INTERFACE_VERSION}"))
    }

    @Test
    fun test_01_ClearAll() {
        LivebaseConnection().use { con ->
            con.printLog = false
            val data = con.connectToServer(
                    DashboardServantConfiguration.fromName(server),
                    username,
                    password,
                    true
            )
            if (data.hasModels()) {
                val resetModels = ResetModelLibraryRequest()
                con.sendRequest<ServantResponse>(resetModels)
            }
            if (data.hasWorkgroups()) {
                val resetCloudlets = ResetDashboardCloudletsRequest()
                con.sendRequest<ServantResponse>(resetCloudlets)
            }
        }
    }

    @Test
    fun test_10_UploadModel() {
        val model = javaClass.getResource("/SampleModel.xml")!!.file
        val file = File(model)
        testTask("""
            uploadModel {
              modelDescription = 'MyModel'
              modelComments = 'MyModel'
              inputFile = '${file.absolutePath}'
            }
        """.trimIndent(), "uploadModel"
        )
    }

    @Test
    fun test_12_DownloadModelGraphQL() {
        val zip = Files.createTempFile("graphql", ".zip").toFile()
        testTask("""
            downloadModelGraphQL {
                modelName = '$modelName'
                outputFile = '${zip.absolutePath}'
            }
            """.trimIndent(), "downloadModelGraphQL")
        assertTrue(zip.exists())
    }

    @Test
    fun test_20_CreateCloudlet() {
        testTask("createCloudlet")
    }

    @Test
    fun test_21_CreateAdmin() {
        testTask("createAdmin")
    }

    @Test
    fun test_23_UploadEngine() {
        val model = javaClass.getResource("/SampleModel.xml")!!.file
        val file = File(model)
        testTask("""
            uploadEngine {
              inputFile = '${file.absolutePath}'
            }
        """.trimIndent(), "uploadEngine")
    }

    @Test
    fun test_24_DeleteEngine() {
        testTask("deleteEngine")
    }

    @Test
    fun test_26_DeployEngine() {
        testTask("""
            deployEngine {
              sourceModelName = '$modelName'
            }
        """.trimIndent(), "deployEngine"
        )
    }

    @Test
    fun test_30_CreateDatabase() {
        testTask("createDatabase")
    }

    @Test
    fun test_32_DownloadEngineGraphQL() {
        val zip = Files.createTempFile("graphql", ".zip").toFile()
        testTask("""
            downloadEngineGraphQL {
                cloudletName = '$cloudletName'
                outputFile = '${zip.absolutePath}'
            }
            """.trimIndent(), "downloadEngineGraphQL")
        assertTrue(zip.exists())
    }

    @Test
    fun test_34_DownloadEngineSpi() {
        val zip = Files.createTempFile("engineSpi", ".zip").toFile()
        testTask("""
            downloadEngineSpi {
                cloudletName = '$cloudletName'
                outputFile = '${zip.absolutePath}'
            }
        """.trimIndent(), "downloadEngineSpi")
        assertTrue(zip.exists())
    }

    @Test
    fun test_40_CheckEngineDatabaseMismatch() {
        testTask("""
            checkEngineDatabaseMismatch {
                failIfAny = true
            }
            """.trimIndent(), "checkEngineDatabaseMismatch")
    }

    @Test
    fun test_42_ModifyEngine() {
        val model = javaClass.getResource("/SampleModel2.xml")!!.file
        val file = File(model)
        testTask("""
            uploadEngine {
              inputFile = '${file.absolutePath}'
            }
        """.trimIndent(), "uploadEngine"
        )
    }

    @Test
    fun test_44_DownloadUpdatePlan() {
        val file = File(testProjectDir, "updatePlan.sql")
        testTask("""
            downloadDatabaseUpdatePlan {
                outputFile = '${file.absolutePath}'
            }
        """.trimIndent(), "downloadDatabaseUpdatePlan")
        assert(file.length() > 0)
        file.deleteOnExit()
    }

    @Test
    fun test_46_UpdateDatabase() {
        testTask("""
            updateDatabase {
                checkDatabase = false
            }
        """.trimIndent(), "updateDatabase")
    }

    @Test
    fun test_48_DownloadDatabase() {
        val file = File(testProjectDir, "db.sql")
        testTask("""
            downloadDatabase {
                outputFile = '${file.absolutePath}'
            }
        """.trimIndent(), "downloadDatabase")
        assert(file.length() > 0)
        file.deleteOnExit()
    }

    @Test
    fun test_50_EnableGraphQL() {
        testTask("enableGraphQL")
    }

    @Test
    fun test_52_ClearDatabase() {
        testTask("clearDatabase")
        testTask("createAdmin")
    }

    @Test
    fun test_60_StartCloudlet() {
        testTask("startCloudlet")
    }

    @Test
    fun test_65_StopCloudlet() {
        testTask("stopCloudlet")
    }

    @Test
    fun test_70_DeleteModel() {
        testTask("deleteModel")
    }

    @Test
    fun test_99_DeleteCloudlet() {
        testTask("deleteCloudlet")
    }
}

private fun LivebaseConnection.use(block: (LivebaseConnection) -> Unit) {
    try {
        block(this)
    } finally {
        try {
            closeConnection()
        } catch (ignored: Exception) {
        }
    }
}
