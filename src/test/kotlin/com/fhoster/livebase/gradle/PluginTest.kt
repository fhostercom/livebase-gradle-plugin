/**
 *    Copyright 2024 Fhoster srl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase.gradle

import com.fhoster.livebase.common.databaseUpdater.SeverityLevel
import com.fhoster.livebase.gradle.tasks.cloudlet.database.CheckDatabaseTask
import com.fhoster.livebase.gradle.tasks.cloudlet.RetrieveCloudletInfoTask
import com.fhoster.livebase.gradle.tasks.cloudlet.StartCloudletTask
import com.fhoster.livebase.gradle.tasks.plugins.StartPluginsTask
import org.gradle.api.internal.project.DefaultProject
import org.gradle.testfixtures.ProjectBuilder
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PluginTest {

    lateinit var prj: DefaultProject
    lateinit var ext: LivebasePluginExtension

    @Before
    fun applyPlugin() {
        prj = ProjectBuilder.builder().build() as DefaultProject
        prj.pluginManager.apply("com.fhoster.livebase")
        ext = prj.extensions.getByName("livebase") as LivebasePluginExtension
        ext.validation()
        ext.setCloudletName("Test")
        ext.setPluginsName("Plugin")
    }

    @Test
    fun testRegEx() {
        Assert.assertTrue("test.*\\.jar".toRegex().containsMatchIn("test.jar"))
        val task = prj.tasks.getByName("deletePlugins")
        Assert.assertTrue(task.hasProperty("needsLock"))
    }

    @Test
    fun testPluginAppliedTest() {
        val task = prj.tasks.getByName("startPlugins")
        Assert.assertTrue(task is StartPluginsTask)
        Assert.assertTrue(prj.tasks.getByName("retrieveCloudletInfo") is RetrieveCloudletInfoTask)
    }

    @Test
    fun testSubtaskInheritExt() {
        val task1 = prj.tasks.getByName("startCloudlet") as StartCloudletTask
        val task2 = prj.tasks.create("subTask", StartCloudletTask::class.java) as StartCloudletTask
        prj.evaluate()
        Assert.assertNotNull(task1)
        Assert.assertEquals(ext.cloudletResolver.cloudletIdentifier, task1.cloudletIdentifier)
        Assert.assertNotNull(task2)
        Assert.assertEquals(ext.cloudletResolver.cloudletIdentifier, task2.cloudletIdentifier)
    }

    @Test
    fun testCheckIssueShouldFail() {
        Assert.assertFalse(CheckDatabaseTask.FailOn.Never.shouldFail(SeverityLevel.HIGH))
        Assert.assertFalse(CheckDatabaseTask.FailOn.Never.shouldFail(SeverityLevel.MEDIUM))
        Assert.assertFalse(CheckDatabaseTask.FailOn.Never.shouldFail(SeverityLevel.LOW))
        Assert.assertFalse(CheckDatabaseTask.FailOn.Never.shouldFail(null))

        Assert.assertTrue(CheckDatabaseTask.FailOn.HighIssue.shouldFail(SeverityLevel.HIGH))
        Assert.assertFalse(CheckDatabaseTask.FailOn.HighIssue.shouldFail(SeverityLevel.MEDIUM))
        Assert.assertFalse(CheckDatabaseTask.FailOn.HighIssue.shouldFail(SeverityLevel.LOW))
        Assert.assertFalse(CheckDatabaseTask.FailOn.HighIssue.shouldFail(null))

        Assert.assertTrue(CheckDatabaseTask.FailOn.AllIssues.shouldFail(SeverityLevel.HIGH))
        Assert.assertTrue(CheckDatabaseTask.FailOn.AllIssues.shouldFail(SeverityLevel.MEDIUM))
        Assert.assertTrue(CheckDatabaseTask.FailOn.AllIssues.shouldFail(SeverityLevel.LOW))
        Assert.assertFalse(CheckDatabaseTask.FailOn.AllIssues.shouldFail(null))
    }

    @After
    fun disconnect() {
        ext.disconnect()
    }
}
